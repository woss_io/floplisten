angular.module('flop', [
  'flop.templates',
  'ngCordova',
  'ionic',
  'ui.router',
  'firebase',
  'angularGeoFire'
]);
