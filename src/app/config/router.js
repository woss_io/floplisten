angular.module('flop').config(function($locationProvider, $urlRouterProvider, $sceDelegateProvider) {
  $locationProvider.html5Mode(false);
  $sceDelegateProvider.resourceUrlWhitelist(['self']);
  $urlRouterProvider.otherwise('/home');
}).run(function($log, $rootScope, $state, $location,$ionicPlatform) {
  $rootScope.$state = $state;
  $rootScope.$on('$stateChangeStart', function(event, toState) {
    if (angular.isDefined(toState.data) && angular.isDefined(toState.data.displayName)) {
      $rootScope.title = toState.data.displayName;
    }
  });
  $rootScope.$on('$stateChangeSuccess', function(event, toState) {
    $rootScope.pageClass = 'page-' + toState.name.replace('.', '-');
  });
  $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
    var status = _.get(error, 'status');
    $log.log('State change error:', status);
    $log.log(error);
    event.preventDefault();
  });
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs).
    // The reason we default this to hidden is that native apps don't usually show an accessory bar, at
    // least on iOS. It's a dead giveaway that an app is using a Web View. However, it's sometimes
    // useful especially with forms, though we would prefer giving the user a little more room
    // to interact with the app.
    if (window.cordova && cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // Set the statusbar to use the default style, tweak this to
      // remove the status bar on iOS or change it to use white instead of dark colors.
      StatusBar.styleDefault();
    }
  });
});
