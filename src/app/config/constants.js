angular.module("flop")

.constant("DEBUG", true)

.constant("API_URL", "")

.constant("API_VERSION", "v1")

.constant("OAUTH2_CLIENT_ID", "flop")

.constant("OAUTH2_CLIENT_SECRET", "secret")

.constant("firebaseLink", "https://floplisten.firebaseio.com")

;