angular.module('flop').config(($stateProvider) => {
  $stateProvider.state('home', {
    url: '/home',
    controller: 'AppController',
    templateUrl: 'components/home/home.html',
    data: {
      displayName: 'Login'
    }
  });
}).controller('AppController', ($log, $scope, $cordovaGeolocation, $cordovaAppAvailability, $ionicPlatform, DeviceService, $document, FirebaseService, SongsService) => {
  let identifier = '';
  /**
   * Songs that are in radius
   */
  $scope.songs = [];

  /**
   * Play a song
   * @param  {[type]} song [description]
   * @return {[type]}      [description]
   */
  $scope.playSong = (song) => {
    console.log('This should play a song', song);
  };

  /**
   * Device ready
   * @param  {[type]} ( [description]
   * @return {[type]}   [description]
   */
  $ionicPlatform.ready(() => {

    if (DeviceService.isIOS)
      identifier = 'spotify://'
    else {
      identifier = 'com.spotify.music'
    }

    $cordovaAppAvailability.check(identifier)
      .then(function() {
        console.log('spotify installed');
        window.mediaNotifications.addEventListener("com.spotify.music.metadatachanged", function(event) {
          SongsService.saveSong(event);
          // $scope.$apply();
        });
      }, function() {
        console.log(identifier + ' is not installed!!');
      });

    var posOptions = {
      timeout: 10000,
      enableHighAccuracy: false
    };
    $cordovaGeolocation
      .getCurrentPosition(posOptions)
      .then(function(position) {
        var lat = position.coords.latitude;
        var long = position.coords.longitude;
        $log.log(lat, long);

      }, function(err) {
        // error
      });

    // end of device ready
  });

});
