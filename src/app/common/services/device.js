angular.module('flop').factory('DeviceService', ($log, $q, $rootScope, $cordovaDevice, $ionicPlatform) => {
  let service = {};
  $ionicPlatform.ready(() => {
    service.deviceInformation = ionic.Platform.device();
    service.isWebView = ionic.Platform.isWebView();
    service.isIPad = ionic.Platform.isIPad();
    service.isIOS = ionic.Platform.isIOS();
    service.isAndroid = ionic.Platform.isAndroid();
    service.isWindowsPhone = ionic.Platform.isWindowsPhone();
    service.currentPlatform = ionic.Platform.platform();
    service.currentPlatformVersion = ionic.Platform.version();

    console.log(service);

  });

  return service;
});
