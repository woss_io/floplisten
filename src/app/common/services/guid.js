angular.module('flop').factory('GUIDService', ($log, $q, FirebaseService) => {
  function s4() {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
  }
  let service = {
    guid: '',
    new: () => {
      // then to call it, plus stitch in '4' in the third group
      guid = (s4() + s4() + "-" + s4() + "-4" + s4().substr(0, 3) + "-" + s4() + "-" + s4() + s4() + s4()).toLowerCase();
      this.guid = guid;
      return guid;

    }
  };
  return service;
});
