angular.module('flop').factory('FirebaseService', ($log, $firebaseObject,firebaseLink,$firebaseArray) => {
  let songs = [];
  let service = {
    baseRef: new Firebase(firebaseLink),
    newRef: function(path){
      let delimiter = '';
      $log.log(firebaseLink[firebaseLink.length - 1]);
      if( firebaseLink[firebaseLink.length - 1] === '/' )
      {
        delimiter = '/';
      }
      let url = firebaseLink + delimiter + path;
      $log.debug('New Firebase REF requested',url);
      return new Firebase(url);
    },
    getSongs: function(){
      let $ref =  this.newRef('songs');
      return $firebaseArray($ref);
    }
  };

  return service;
});
