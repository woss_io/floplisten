var gulp = require('gulp');
var zip = require('gulp-zip');
var rename = require('gulp-rename');
var awsS3 = require('gulp-aws-s3');
var runSequence = require('gulp-run-sequence');
var paths = require('./paths');
//var zipFile = 'client_TIMESTAMP.zip'.replace('TIMESTAMP', Date.now());
var zipFile = 'client-latest.zip';

gulp.task('zip', ['pre:zip'], function() {
  return gulp.src(paths.public + '/**/*')
    .pipe(zip(zipFile))
    .pipe(gulp.dest(paths.releases));
});

gulp.task('pre:zip', function() {
  return runSequence('build:prod');
});

gulp.task('deploy', [ 'zip'], function() {
  return gulp.src(paths.releases + '/' + zipFile)
    .pipe(rename('client-latest.zip'))
    .pipe(awsS3.upload({path: 'odin/'}, {
      key: process.env.S3_ACCESS_KEY,
      secret: process.env.S3_SECRET,
      region: 'eu-west-1',
      bucket: 'gamerefinery-client-deployments'
    }));
});
