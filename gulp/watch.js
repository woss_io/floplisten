var gulp = require('gulp'),
  paths = require('./paths');
gulp.task('watch', ['build:dev'], function() {
  gulp.watch(paths.app, ['app']);
  gulp.watch(paths.html, ['html']);
  gulp.watch(paths.templates, ['templates']);
  gulp.watch(paths.sass, ['sass']);
  gulp.watch(paths.vendor, ['vendor']);
  gulp.watch(paths.fonts, ['fonts']);
  gulp.watch(paths.images, ['images']);
});
