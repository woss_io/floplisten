var gulp = require('gulp');
var concat = require('gulp-concat');
var paths = require('./paths');
var debug = require('gulp-debug');

gulp.task('vendor', function() {
  return gulp.src(paths.vendor)
      .pipe(concat('vendor.js'))
      .pipe(debug({title: 'flopApp:'}))
      .pipe(gulp.dest(paths.public + '/js/'));
});
