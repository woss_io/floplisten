var gulp = require('gulp');
var protractor = require('gulp-protractor');
var paths = require('./paths');

gulp.task('webdriver:start', ['webdriver:update'], protractor.webdriver);
gulp.task('webdriver:update', protractor.webdriver_update);
gulp.task('webdriver:standalone', ['webdriver:update'], protractor.webdriver_standalone);

gulp.task('test', ['build:dev', 'webdriver:start'], function() {
  return gulp.src(paths.spec + '/**/*Spec.js')
    .pipe(protractor.protractor({
      configFile: 'protractor.conf.js'
    }))
    .on('error', function(err) {
      console.log(err);
    });
});
