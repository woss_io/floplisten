var gulp = require('gulp');
var paths = require('./paths');
var concat = require('gulp-concat');
var ngAnnotate = require('gulp-ng-annotate');
var debug = require('gulp-debug');
var babel = require('gulp-babel');

gulp.task('app', function() {
  return gulp.src(paths.app)
      .pipe(concat('app.js'))
      .pipe(babel())
      .pipe(ngAnnotate({
        single_quotes: true
      }))
      .pipe(debug({title: 'flop:'}))
      .pipe(gulp.dest(paths.public + '/js/'));
});
